// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import VueMdl from 'vue-mdl'

Vue.use(Vuex)
Vue.use(VueMdl)

import App from './App'
import router from './router'

const store = new Vuex.Store({
  state: {
    file: '',
    nightMode: false,
    reverseMode: false,
    charPerLine: 60
  },

  mutations: {
    toggleNight (state) {
      state.nightMode = !state.nightMode
    },

    toggleReverse (state) {
      state.reverseMode = !state.reverseMode
    },

    loadFile (state, file) {
      state.file = file
    },

    setCharPerLine (state, charPerLine) {
      state.charPerLine = parseInt(charPerLine)
    }
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
