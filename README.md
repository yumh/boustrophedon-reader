# boustrophedon reader

> A Vue.js project

## Disclaimer

This is an hobby project to primary test (and learn) `vue.js`, and for fun.

The code may not be perfect, nor it is always supposed to be. It's a hobby!

Said that, here's a cool
[boustrophedon](https://en.wikipedia.org/wiki/Boustrophedon) reader that you should try!

## Requirements

 - Nodejs and npm/yarn

## Getting started

```sh
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## TODOs

> order matters

#### short-term

- [X] use vuex!
- [X] handle negative input
- [X] apply night mode to the body, not elsewhere
- [ ] add photo to this repo
- [X] better UI for settings
- [ ] support for pagination

#### mid/long term

- [ ] support for markdown files
- [ ] support for EPUB
- [ ] ...
- [ ] wrap everything inside [Electron](https://github.com/electron/electron)
